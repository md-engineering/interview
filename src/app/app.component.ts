import { Component } from '@angular/core';

@Component({
  selector: 'helix-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {

  measurementsData = '';

  onMeasurementAdded(eventData: string) {
    this.measurementsData = this.measurementsData + eventData;
  }

}
