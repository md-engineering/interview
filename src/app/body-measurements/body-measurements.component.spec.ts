import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BodyMeasurementsComponent } from './body-measurements.component';
import { FormsModule } from '@angular/forms';
import { MeasurementsService } from '../services/measurements.service';
import { inject } from '@angular/core/testing';
import { headersToString } from 'selenium-webdriver/http';

describe('BodyMeasurementsComponent', () => {
  let component: BodyMeasurementsComponent;
  let fixture: ComponentFixture<BodyMeasurementsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [BodyMeasurementsComponent],
        imports: [FormsModule],
        providers: [MeasurementsService]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyMeasurementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  [
    [105.4, 16.9],
    [99.1, 16.6],
    [103.5, 18.3]
  ].forEach(([height, weight]) => {
    it(
      `should emit expected html for height ${height} and weight ${weight}`,
      inject([MeasurementsService], (service: MeasurementsService) => {
        component.height = height;
        component.weight = weight;
        const expectedBmi = service.CalculateBmi(height, weight);

        component.measurementAdded.subscribe(x => {
          expect(x).toEqual(
            '<tr><td>Height: ' +
              height +
              '</td><td>Weight: ' +
              weight +
              '</td><td>BMI: ' +
              expectedBmi +
              '</td></tr>'
          );
        });

        component.addMeasurement();
      })
    );
  });
});
