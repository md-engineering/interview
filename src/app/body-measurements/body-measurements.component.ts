import { Component, OnInit } from '@angular/core';
import { EventEmitter, Output } from '@angular/core';
import { MeasurementsService } from '../services/measurements.service';

@Component({
  selector: 'helix-body-measurements',
  templateUrl: './body-measurements.component.html',
  styleUrls: ['./body-measurements.component.css']
})
export class BodyMeasurementsComponent {
  height: number;
  weight: number;

  @Output() measurementAdded: EventEmitter<any> = new EventEmitter();

  constructor(private measurementsService: MeasurementsService) {}

  addMeasurement() {
    this.measurementAdded.emit(
      '<tr><td>Height: ' +
        this.height +
        '</td><td>Weight: ' +
        this.weight +
        '</td><td>BMI: ' +
        this.measurementsService.CalculateBmi(this.height, this.weight) +
        '</td></tr>'
    );
  }
}
