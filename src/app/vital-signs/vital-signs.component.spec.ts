import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VitalSignsComponent } from './vital-signs.component';
import { FormsModule } from '@angular/forms';

describe('VitalSignsComponent', () => {
  let component: VitalSignsComponent;
  let fixture: ComponentFixture<VitalSignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VitalSignsComponent ],
      imports: [
        FormsModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitalSignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit correct html', () => {

        component.measurementAdded.subscribe(x => {
          expect(x).toEqual('<tr><td>Pulse: 60</td><td>Blood Pressure: 120/80</td></tr>');
        });

        component.pulse = 60;
        component.bloodPressure = '120/80';
        component.addMeasurement();
      });
});
