import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'helix-vital-signs',
  templateUrl: './vital-signs.component.html',
  styleUrls: ['./vital-signs.component.css']
})
export class VitalSignsComponent implements OnInit {

  pulse: number;
  bloodPressure: string;

  @Output() measurementAdded: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  addMeasurement() {
    this.measurementAdded.emit('<tr><td>Pulse: ' + this.pulse + '</td><td>Blood Pressure: ' + this.bloodPressure + '</td></tr>');
  }
}
