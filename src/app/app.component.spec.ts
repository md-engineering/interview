import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BodyMeasurementsComponent } from './body-measurements/body-measurements.component';
import { MeasurementsGridComponent } from './measurements-grid/measurements-grid.component';
import { VitalSignsComponent } from './vital-signs/vital-signs.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MeasurementsService } from './services/measurements.service';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        BodyMeasurementsComponent,
        VitalSignsComponent,
        MeasurementsGridComponent,
      ],
      imports: [
        BrowserModule,
        FormsModule
      ],
      providers: [MeasurementsService]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
