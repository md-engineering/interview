import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementsGridComponent } from './measurements-grid.component';

describe('MeasurementsGridComponent', () => {
  let component: MeasurementsGridComponent;
  let fixture: ComponentFixture<MeasurementsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasurementsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasurementsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
