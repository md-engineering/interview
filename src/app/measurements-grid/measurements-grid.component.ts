import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'helix-measurements-grid',
  templateUrl: './measurements-grid.component.html',
  styleUrls: ['./measurements-grid.component.css']
})
export class MeasurementsGridComponent implements OnInit {

  @Input() public measurementsData: string;

  constructor() { }

  ngOnInit() {
  }

}
