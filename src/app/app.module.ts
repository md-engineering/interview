import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BodyMeasurementsComponent } from './body-measurements/body-measurements.component';
import { MeasurementsGridComponent } from './measurements-grid/measurements-grid.component';
import { VitalSignsComponent } from './vital-signs/vital-signs.component';
import { MeasurementsService } from './services/measurements.service';

@NgModule({
  declarations: [
    AppComponent,
    BodyMeasurementsComponent,
    MeasurementsGridComponent,
    VitalSignsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [MeasurementsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
