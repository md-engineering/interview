import { Injectable } from '@angular/core';

@Injectable()
export class MeasurementsService {

  constructor() { }

  public CalculateBmi(height: number, weight: number): number {
    return (weight / height / height) * 10000;
  }
}
