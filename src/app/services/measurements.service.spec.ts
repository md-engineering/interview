import { TestBed, inject } from '@angular/core/testing';

import { MeasurementsService } from './measurements.service';

describe('MeasurementsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeasurementsService]
    });
  });

  it(
    'should be created',
    inject([MeasurementsService], (service: MeasurementsService) => {
      expect(service).toBeTruthy();
    })
  );

  [
    [105.4, 16.9, 15.2],
    [99.1, 16.6, 16.9],
    [103.5, 18.3, 17.1]
  ].forEach(([height, weight, expectedBmi]) => {
    it(
      `should return BMI ${expectedBmi} for height ${height} and weight ${weight}`,
      inject([MeasurementsService], (service: MeasurementsService) => {
        expect(service.CalculateBmi(height, weight)).toBe(expectedBmi);
      })
    );
  });
});
