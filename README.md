# Patient Metrics

## Background

A junior developer on your team has begun work on a front-end prototype for a new patient metrics component.

She has used Angular CLI to quickly create an Angular 5 project with components, services, and tests.

The prototype works mostly as specified, but there are some failing tests! See if you can help her get the tests passing.

Are there any issues with how this has been implemented? Could you offer any advice to improve her design?

# Specifications

## Body measurements component

Add a component with inputs for height & weight

Inputs must only be numbers

## Vital signs component

Add a component with inputs for pulse & blood pressure

Inputs must only be numbers

Blood pressure must be in the form of systolic / diastolic  (e.g. 120/80) (no bounds on the numbers)

## Measurements table component

Add a component with a table to display inputs from the other two components

Add an 'add' button to both body measurements & vital signs components

When clicking 'add', the measurements should be added as a new row to the table

'Add' button should be disabled until inputs are valid

The table can contain both body measurement and vital signs information

## Add BMI to the vital signs information in the measurements table

In addition to height & weight, calculate and add BMI to each vital signs measurement in the measurements table. Calculate the BMI to one decimal place.

The formula for BMI is:
[weight (kg) / height (cm) / height (cm)] x 10,000